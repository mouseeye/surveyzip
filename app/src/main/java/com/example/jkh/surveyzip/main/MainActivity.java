package com.example.jkh.surveyzip.main;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.jkh.surveyzip.DBHelper;
import com.example.jkh.surveyzip.R;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity {

    DBHelper mHelper= new DBHelper(this) ;
    Timer timer_logo; // 로고출력 타이머

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#006064"));
        }

        TimerTask task_change = new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, SurveyActivity.class);
                start_Activity_NOTDUP(intent);
                finish();
            }
        };
        timer_logo = new Timer();
        timer_logo.schedule(task_change, 2000);
    }

    /*************************************************************************************************************/

    // back 버튼 이벤트
    private boolean isSecond = false;
    private Timer timer_finish;
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if( keyCode == KeyEvent.KEYCODE_BACK ){
            if( isSecond == false){
                Toast.makeText(this, "한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
                isSecond = true;

                TimerTask task_finish = new TimerTask() {
                    @Override
                    public void run() {
                        timer_finish.cancel();
                        timer_finish = null;
                        isSecond = false;
                    }
                };
                if(timer_finish != null){	timer_finish.cancel();	 timer_finish = null;	}
                timer_finish = new Timer();
                timer_finish.schedule(task_finish, 2000);
            }else{
                super.onBackPressed();

                if( timer_logo != null) { timer_logo.cancel(); timer_logo = null; }
                finish();
            }
        }
        return false;
    }

    int flag_intent_on = 0; // Intent 호출 여부
    public void start_Activity_NOTDUP(Intent intent){ // 중복없는 액티비티 호출
        if( flag_intent_on == 0){
            startActivity(intent);
            flag_intent_on = 1;
        }
    }
}