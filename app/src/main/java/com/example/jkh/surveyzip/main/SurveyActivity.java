package com.example.jkh.surveyzip.main;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.jkh.surveyzip.R;
import com.example.jkh.surveyzip.SurveyFragment.Add_Survey;
import com.example.jkh.surveyzip.SurveyFragment.Do_Survey;
import com.example.jkh.surveyzip.SurveyFragment.Result_Survey;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import com.viewpagerindicator.TitlePageIndicator;

public class SurveyActivity extends AppCompatActivity implements ActionBar.TabListener {

    private final int INIT_PAGE_NUM = 1;
    private final int TOTAL_PAGE_NUM = 3;
    Fragment fragment0, fragment1, fragment2;

    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    TitlePageIndicator barIndicator;
    ArrayList<LinearLayout> btn_topmenu_list, btn_topmenu_list_inner;

    float dev_width, dev_height;
    double dev_inch;
    float dev_dpi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#00796B"));
        }

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        dev_width = metrics.widthPixels;
        dev_height = metrics.heightPixels;

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
        double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
        dev_inch = Math.sqrt(x + y);
        dev_dpi = dm.density;

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(0xFF009688));
//        actionBar.setDisplayShowHomeEnabled(false);
//        actionBar.setDisplayShowTitleEnabled(false); // Hide ActionBar, Keep Tabs

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.pager);
        btn_topmenu_list = new ArrayList<LinearLayout>();
        btn_topmenu_list.add((LinearLayout) findViewById(R.id.lay_topmenu0));
        btn_topmenu_list.add((LinearLayout) findViewById(R.id.lay_topmenu1));
        btn_topmenu_list.add((LinearLayout) findViewById(R.id.lay_topmenu2));
        btn_topmenu_list_inner = new ArrayList<LinearLayout>();
        btn_topmenu_list_inner.add((LinearLayout) findViewById(R.id.lay_topmenu0_inner));
        btn_topmenu_list_inner.add((LinearLayout) findViewById(R.id.lay_topmenu1_inner));
        btn_topmenu_list_inner.add((LinearLayout) findViewById(R.id.lay_topmenu2_inner));

        mViewPager.setAdapter(mSectionsPagerAdapter);

        barIndicator = (TitlePageIndicator) findViewById(R.id.titles);
        barIndicator.setFooterLineHeight(0);
        barIndicator.setRecwidth(dev_width / TOTAL_PAGE_NUM);
        barIndicator.setFooterColor(Color.parseColor("#00796B")); // Indicator 색상
        barIndicator.setX(dev_width * ((float) INIT_PAGE_NUM / TOTAL_PAGE_NUM));
        barIndicator.setViewPager(mViewPager);

        mViewPager.setCurrentItem(INIT_PAGE_NUM);

        btn_topmenu_list_inner.get(0).setBackgroundResource(R.drawable.btn_tab0_1);
        btn_topmenu_list_inner.get(1).setBackgroundResource(R.drawable.btn_tab1_1);
        btn_topmenu_list_inner.get(2).setBackgroundResource(R.drawable.btn_tab2_1);
        switch( INIT_PAGE_NUM ){
            case 0:  btn_topmenu_list_inner.get(0).setBackgroundResource(R.drawable.btn_tab0_0); break;
            case 1:  btn_topmenu_list_inner.get(1).setBackgroundResource(R.drawable.btn_tab1_0); break;
            case 2:  btn_topmenu_list_inner.get(2).setBackgroundResource(R.drawable.btn_tab2_0); break;
        }

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int position) {
                btn_topmenu_list_inner.get(0).setBackgroundResource(R.drawable.btn_tab0_1);
                btn_topmenu_list_inner.get(1).setBackgroundResource(R.drawable.btn_tab1_1);
                btn_topmenu_list_inner.get(2).setBackgroundResource(R.drawable.btn_tab2_1);
                switch (position) {
                    case 0:
                        btn_topmenu_list_inner.get(0).setBackgroundResource(R.drawable.btn_tab0_0);
                        actionBar.setTitle(R.string.title_section1);
                        break;
                    case 1:
                        btn_topmenu_list_inner.get(1).setBackgroundResource(R.drawable.btn_tab1_0);
                        actionBar.setTitle(R.string.title_section2);
                        if (((Do_Survey) fragment1) != null)
                            ((Do_Survey) fragment1).load_Survey_List();
                        break;
                    case 2:
                        btn_topmenu_list_inner.get(2).setBackgroundResource(R.drawable.btn_tab2_0);
                        actionBar.setTitle(R.string.title_section3);
                        if (((Result_Survey) fragment2) != null)
                            ((Result_Survey) fragment2).load_Survey_Result_List();
                        break;
                    default:
                        actionBar.setTitle(R.string.app_name);
                        return;
                }
                if (((Add_Survey) fragment0) != null) ((Add_Survey) fragment0).hide_keyboard_all();
                if (((Do_Survey) fragment1) != null) ((Do_Survey) fragment1).hide_keyboard_all();
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
                barIndicator.setX(dev_width * ((float) arg0 / TOTAL_PAGE_NUM) + dev_width / TOTAL_PAGE_NUM * arg1); // Indicator Scroll 적용
            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            Bundle args = null;

            switch (position) {
                case 0:
                    fragment0 = new Add_Survey();
                    ((Add_Survey) fragment0).setHandler(handler_frag0);
                    ((Add_Survey) fragment0).setContext(SurveyActivity.this);
                    args = new Bundle();
                    fragment = fragment0;
                    break;

                case 1:
                    fragment1 = new Do_Survey();
                    ((Do_Survey) fragment1).setHandler(handler_frag1);
                    args = new Bundle();
                    fragment = fragment1;
                    break;

                case 2:
                    fragment2 = new Result_Survey();
                    ((Result_Survey) fragment2).setHandler(handler_frag2);
                    args = new Bundle();
                    fragment = fragment2;
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return TOTAL_PAGE_NUM;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }

    public Handler handler_frag0 = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    mViewPager.setCurrentItem(1);
                    break;
            }
        }
    };
    public Handler handler_frag1 = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    mViewPager.setCurrentItem(0);
                    break;
            }
        }
    };
    public Handler handler_frag2 = new Handler() {
        public void handleMessage(Message msg) {
            Intent intent = new Intent(SurveyActivity.this, ResultDetailAct.class);
            intent.putExtra("IDX", msg.what);
            startActivity(intent);
        }
    };

    // 버튼 클릭 리스너
    public void mTabOnClick(View v) {
        switch (v.getId()) {
            case R.id.lay_topmenu0:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.lay_topmenu1:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.lay_topmenu2:
                mViewPager.setCurrentItem(2);
                break;
        }
    }

    /*************************************************************************************************************/

    // back 버튼 이벤트
    private boolean isSecond = false;
    private Timer timer_finish;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isSecond == false) {
                Toast.makeText(this, "한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
                isSecond = true;

                TimerTask task_finish = new TimerTask() {
                    @Override
                    public void run() {
                        timer_finish.cancel();
                        timer_finish = null;
                        isSecond = false;
                    }
                };
                if (timer_finish != null) {
                    timer_finish.cancel();
                    timer_finish = null;
                }
                timer_finish = new Timer();
                timer_finish.schedule(task_finish, 2000);
            } else {
                super.onBackPressed();
                finish();
            }
        }
        return false;
    }

}
