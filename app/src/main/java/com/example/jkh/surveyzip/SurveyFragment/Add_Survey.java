package com.example.jkh.surveyzip.SurveyFragment;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jkh.surveyzip.DBHelper;
import com.example.jkh.surveyzip.R;
import com.example.jkh.surveyzip.UI.CustomViewPager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class Add_Survey extends android.support.v4.app.Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    Context context;
    DBHelper mHelper;
    SQLiteDatabase db;

    private Handler handler = null;

    private CustomViewPager viewPager;
    private PagerAdapterClass mPageAdpater;
    private int cur_page = 0;
    private final int TOTAL_QUE_NUM = 10;
    private final int TOTAL_PAGE_NUM = TOTAL_QUE_NUM + 1;

    private Button btn_pre, btn_next, btn_finish, btn_list;
    private RadioButton rbtn_type0, rbtn_type1;
    private LinearLayout lay_mask, lay_finish;

    public EditText et_title, et_description;
    public TextView tv_et_title, tv_et_description;
    public ArrayList<EditText> list_et_title, list_et_item0, list_et_item1, list_et_item2, list_et_item3, list_et_item4; // idx 0~9
    public ArrayList<LinearLayout> list_lay_allow;

    // 설문등록 정보
    public ArrayList<Integer> list_que_type; // 문항타입
    public String survey_title, survey_description; // 설문제목, 설문설명
    public ArrayList<String> list_que_title; // 질문명
    public ArrayList<String> list_que_item0; // 항목1
    public ArrayList<String> list_que_item1; // 항목2
    public ArrayList<String> list_que_item2; // 항목3
    public ArrayList<String> list_que_item3; // 항목4
    public ArrayList<String> list_que_item4; // 항목5

    public static Add_Survey newInstance(String param1, String param2) {
        Add_Survey fragment = new Add_Survey();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public Add_Survey() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public void setHandler(Handler _handler) {
        handler = _handler;
    }

    public void setContext(Context _context) {
        context = _context;
        mHelper = new DBHelper(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_survey, container, false);

        viewPager = (CustomViewPager) v.findViewById(R.id.view_pager);
        rbtn_type0 = (RadioButton) v.findViewById(R.id.rbtn_type0);
        rbtn_type1 = (RadioButton) v.findViewById(R.id.rbtn_type1);
        lay_mask = (LinearLayout) v.findViewById(R.id.lay_mask);
        lay_finish = (LinearLayout) v.findViewById(R.id.lay_finish);

        list_lay_allow = new ArrayList<LinearLayout>();
        list_lay_allow.add((LinearLayout) v.findViewById(R.id.lay_allow0));
        list_lay_allow.add((LinearLayout) v.findViewById(R.id.lay_allow1));
        list_lay_allow.add((LinearLayout) v.findViewById(R.id.lay_allow2));

        init_input_object();

        rbtn_type0.setChecked(true);
        rbtn_type1.setChecked(false);
        rbtn_type0.setVisibility(View.INVISIBLE);
        rbtn_type1.setVisibility(View.INVISIBLE);
        lay_mask.setVisibility(View.INVISIBLE);
        lay_finish.setVisibility(View.INVISIBLE);

        // '선택형' 라디오버튼
        rbtn_type0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list_que_type.set(cur_page - 1, 0);
                rbtn_type1.setChecked(false);
                lay_mask.setVisibility(View.INVISIBLE);

                enable_que_view(cur_page - 1);
            }
        });
        // '입력형' 체크박스
        rbtn_type1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list_que_type.set(cur_page - 1, 1);
                rbtn_type0.setChecked(false);
                lay_mask.setVisibility(View.VISIBLE);

                disable_que_view(cur_page - 1);
            }
        });

        mPageAdpater = new PagerAdapterClass(getContext());
        viewPager.setAdapter(mPageAdpater);
        viewPager.setCurrentItem(0);

        // '이전' 버튼
        btn_pre = (Button) v.findViewById(R.id.btn_pre);
        btn_pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cur_page = cur_page - 1;
                if (cur_page < 0) {
                    cur_page = 0;
                }
                if (cur_page == 0) {
                    btn_finish.setEnabled(false);
                    setUIAllow(0);
                }
                viewPager.setCurrentItem(cur_page);

                handle_view_by_que_type(cur_page);
            }
        });

        // '다음' 버튼
        btn_next = (Button) v.findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValue() == -1) {
                    Toast.makeText(getContext(), "항목을 입력해주세요.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (checkValue() == -2) {
                    Toast.makeText(getContext(), "최소한 2개 항목을 입력해주세요.", Toast.LENGTH_SHORT).show();
                    return;
                }
                inputValue();

                cur_page = cur_page + 1;
                if (cur_page >= TOTAL_PAGE_NUM) {
                    cur_page = TOTAL_PAGE_NUM - 1;
                }
                if (cur_page != 0) {
                    btn_finish.setEnabled(true);
                    setUIAllow(1);
                }
                viewPager.setCurrentItem(cur_page);

                handle_view_by_que_type(cur_page);
            }
        });

        // '완료' 버튼
        btn_finish = (Button) v.findViewById(R.id.btn_finish);
        btn_finish.setEnabled(false);
        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValue() == -1) {
                    Toast.makeText(getContext(), "항목을 입력해주세요.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (checkValue() == -2) {
                    Toast.makeText(getContext(), "최소한 2개 항목을 입력해주세요.", Toast.LENGTH_SHORT).show();
                    return;
                }
                inputValue();

                int max_idx = -1;
                int db_m_idx;
                db = mHelper.getReadableDatabase();
                Cursor cursor = db.rawQuery("SELECT max(idx) FROM survey_list", null);
                while (cursor.moveToNext()) {
                    max_idx = cursor.getInt(0);
                }
                if (max_idx == -1) db_m_idx = 0;
                else db_m_idx = max_idx + 1;

                int db_que_num = 0;
                for (int i = 0; i < TOTAL_QUE_NUM; i++) {
                    if (list_que_title.get(i) == null) break;
                    String str_query = "INSERT into survey_que (idx, que_idx, type, que_title, que_items) values " +
                            "(" + db_m_idx + ", " + i + ", " + list_que_type.get(i) + ", '" + list_que_title.get(i) + "', " +
                            "'" + list_que_item0.get(i) + "@@" + list_que_item1.get(i) + "@@" + list_que_item2.get(i) + "@@" + list_que_item3.get(i) + "@@" + list_que_item4.get(i) + "');";
                    db.execSQL(str_query);
                    db_que_num++;
                }

                db = mHelper.getWritableDatabase();
                db.execSQL("INSERT into survey_list (idx, title, description, que_date, que_num) values " +
                        "(" + db_m_idx + ", '" + survey_title + "', '" + survey_description + "'," +
                        "'" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())) + "', " + db_que_num + ")");

                viewPager.setVisibility(View.INVISIBLE);
                rbtn_type0.setVisibility(View.INVISIBLE);
                rbtn_type1.setVisibility(View.INVISIBLE);
                lay_mask.setVisibility(View.INVISIBLE);
                lay_finish.setVisibility(View.VISIBLE);

                btn_finish.setEnabled(false);
                btn_next.setEnabled(false);
                btn_pre.setEnabled(false);

                setUIAllow(2);
            }
        });

        // '목록보기' 버튼
        btn_list = (Button) v.findViewById(R.id.btn_list);
        btn_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message msg = new Message();
                msg.what = 0;
                handler.sendMessage(msg);

                init_view();
            }
        });
        return v;
    }

    public void setUIAllow(int idx) {
        for (int i = 0; i < list_lay_allow.size(); i++) {
            list_lay_allow.get(i).setBackgroundResource(R.drawable.img_allow_1);
        }
        list_lay_allow.get(idx).setBackgroundResource(R.drawable.img_allow_0);
    }

    // CHECK Value Validation
    public int checkValue() {
        if (cur_page == 0) {
            if (et_title.getText().toString().trim().equals("") || et_description.getText().toString().equals(""))
                return -1;
            else return 1;
        }
        if (list_et_title.get(cur_page - 1).getText().toString().trim().equals("")) return -1;
        if (list_que_type.get(cur_page - 1) == 1) {
            return 1;
        }
        if (list_et_item0.get(cur_page - 1).getText().toString().trim().equals("")) return -2;
        if (list_et_item1.get(cur_page - 1).getText().toString().trim().equals("")) return -2;

        return 1;
    }

    // Input Value
    public void inputValue() {
        if (cur_page == 0) {
            survey_title = et_title.getText().toString();
            survey_description = et_description.getText().toString();
            return;
        }
        if (list_et_title.get(cur_page - 1) != null && list_et_title.get(cur_page - 1).getText().toString().length() != 0)
            list_que_title.set(cur_page - 1, list_et_title.get(cur_page - 1).getText().toString());
        if (list_et_item0.get(cur_page - 1) != null && list_et_item0.get(cur_page - 1).getText().toString().length() != 0)
            list_que_item0.set(cur_page - 1, list_et_item0.get(cur_page - 1).getText().toString());
        if (list_et_item1.get(cur_page - 1) != null && list_et_item1.get(cur_page - 1).getText().toString().length() != 0)
            list_que_item1.set(cur_page - 1, list_et_item1.get(cur_page - 1).getText().toString());
        if (list_et_item2.get(cur_page - 1) != null && list_et_item2.get(cur_page - 1).getText().toString().length() != 0)
            list_que_item2.set(cur_page - 1, list_et_item2.get(cur_page - 1).getText().toString());
        if (list_et_item3.get(cur_page - 1) != null && list_et_item3.get(cur_page - 1).getText().toString().length() != 0)
            list_que_item3.set(cur_page - 1, list_et_item3.get(cur_page - 1).getText().toString());
        if (list_et_item4.get(cur_page - 1) != null && list_et_item4.get(cur_page - 1).getText().toString().length() != 0)
            list_que_item4.set(cur_page - 1, list_et_item4.get(cur_page - 1).getText().toString());
    }

    public void hide_keyboard_all() {
        if (et_title != null) hide_keyboard(et_title);
        if (et_description != null) hide_keyboard(et_description);
        if (list_et_item0 != null) {
            for (int i = 0; i < list_et_item0.size(); i++) {
                hide_keyboard(list_et_item0.get(i));
            }
        }
    }

    public void hide_keyboard(EditText et) {
        if (et == null) return;
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
    }

    class PagerAdapterClass extends PagerAdapter {
        private LayoutInflater mInflater;

        public PagerAdapterClass(Context c) {
            super();
            mInflater = LayoutInflater.from(c);
        }

        @Override
        public int getCount() {
            return TOTAL_PAGE_NUM;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

        @Override
        public Object instantiateItem(View pager, int position) {
            View v = null;
            if (position == 0) {
                v = mInflater.inflate(R.layout.inflate_survey_add_0, null);

                et_title = (EditText) v.findViewById(R.id.et_title);
                et_description = (EditText) v.findViewById(R.id.et_description);
                tv_et_title = (TextView) v.findViewById(R.id.tv_et_title);
                tv_et_description = (TextView) v.findViewById(R.id.tv_et_description);

                et_title.addTextChangedListener(new TextWatcher() {
                    public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                        int input_size = et_title.getText().toString().length();
                        if (input_size == 0) {
                            tv_et_title.setText("ex) 구매 관련 설문");
                        } else if (tv_et_title.getText().toString().length() != 0) {
                            tv_et_title.setText("");
                        }
                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {
                    }
                });
                et_description.addTextChangedListener(new TextWatcher() {
                    public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                        int input_size = et_description.getText().toString().length();
                        if (input_size == 0) {
                            tv_et_description.setText("ex) 2015년 10월 구매관련 설문입니다.");
                        } else if (tv_et_description.getText().toString().length() != 0) {
                            tv_et_description.setText("");
                        }
                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {
                    }
                });

                // Load Value
                if (survey_title != null) et_title.setText(survey_title);
                if (survey_description != null) et_description.setText(survey_description);

            } else {
                v = mInflater.inflate(R.layout.inflate_survey_add_1, null);
                ((TextView) v.findViewById(R.id.tv_title)).setText(position + "번 질문");

                list_et_title.set(position - 1, (EditText) v.findViewById(R.id.et_title));
                list_et_item0.set(position - 1, (EditText) v.findViewById(R.id.et_item0));
                list_et_item1.set(position - 1, (EditText) v.findViewById(R.id.et_item1));
                list_et_item2.set(position - 1, (EditText) v.findViewById(R.id.et_item2));
                list_et_item3.set(position - 1, (EditText) v.findViewById(R.id.et_item3));
                list_et_item4.set(position - 1, (EditText) v.findViewById(R.id.et_item4));

                // Load Value
                if (list_que_title.get(position - 1) != null)
                    list_et_title.get(position - 1).setText(list_que_title.get(position - 1));
                if (list_que_item0.get(position - 1) != null)
                    list_et_item0.get(position - 1).setText(list_que_item0.get(position - 1));
                if (list_que_item1.get(position - 1) != null)
                    list_et_item1.get(position - 1).setText(list_que_item1.get(position - 1));
                if (list_que_item2.get(position - 1) != null)
                    list_et_item2.get(position - 1).setText(list_que_item2.get(position - 1));
                if (list_que_item3.get(position - 1) != null)
                    list_et_item3.get(position - 1).setText(list_que_item3.get(position - 1));
                if (list_que_item4.get(position - 1) != null)
                    list_et_item4.get(position - 1).setText(list_que_item4.get(position - 1));
            }
            ((CustomViewPager) pager).addView(v, 0);
            return v;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public boolean isViewFromObject(View pager, Object obj) {
            return pager == obj;
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
        }

        @Override
        public void destroyItem(View pager, int position, Object view) {
            if (position == 0) {
            }
            ((CustomViewPager) pager).removeView((View) view);
        }

        @Override
        public void finishUpdate(View arg0) {
        }
    }

    // Que Input 객체 초기화
    public void init_input_object() {
        if (list_et_title == null) list_et_title = new ArrayList<EditText>();
        if (list_et_item0 == null) list_et_item0 = new ArrayList<EditText>();
        if (list_et_item1 == null) list_et_item1 = new ArrayList<EditText>();
        if (list_et_item2 == null) list_et_item2 = new ArrayList<EditText>();
        if (list_et_item3 == null) list_et_item3 = new ArrayList<EditText>();
        if (list_et_item4 == null) list_et_item4 = new ArrayList<EditText>();

        survey_title = null;
        survey_description = null;
        if (list_que_type == null) list_que_type = new ArrayList<Integer>();
        if (list_que_title == null) list_que_title = new ArrayList<String>();
        if (list_que_item0 == null) list_que_item0 = new ArrayList<String>();
        if (list_que_item1 == null) list_que_item1 = new ArrayList<String>();
        if (list_que_item2 == null) list_que_item2 = new ArrayList<String>();
        if (list_que_item3 == null) list_que_item3 = new ArrayList<String>();
        if (list_que_item4 == null) list_que_item4 = new ArrayList<String>();

        list_et_title.clear();
        list_et_item0.clear();
        list_et_item1.clear();
        list_et_item2.clear();
        list_et_item3.clear();
        list_et_item4.clear();

        list_que_type.clear();
        list_que_title.clear();
        list_que_item0.clear();
        list_que_item1.clear();
        list_que_item2.clear();
        list_que_item3.clear();
        list_que_item4.clear();

        for (int i = 0; i < TOTAL_QUE_NUM; i++) {
            list_et_title.add(null);
            list_et_item0.add(null);
            list_et_item1.add(null);
            list_et_item2.add(null);
            list_et_item3.add(null);
            list_et_item4.add(null);
            list_que_type.add(0);
            list_que_title.add(null);
            list_que_item0.add(null);
            list_que_item1.add(null);
            list_que_item2.add(null);
            list_que_item3.add(null);
            list_que_item4.add(null);
        }
    }

    // View 초기화
    public void init_view() {
        init_input_object();

        // ViewPager 초기화
        viewPager.removeAllViews();
        viewPager.setAdapter(mPageAdpater);
        viewPager.setCurrentItem(0);
        viewPager.setVisibility(View.VISIBLE);

        cur_page = 0;
        lay_mask.setVisibility(View.INVISIBLE);
        lay_finish.setVisibility(View.INVISIBLE);

        btn_finish.setEnabled(false);
        btn_next.setEnabled(true);
        btn_pre.setEnabled(true);
        setUIAllow(0);
    }

    public void handle_view_by_que_type(int _cur_page) {
        if (_cur_page == 0) {
            rbtn_type0.setVisibility(View.INVISIBLE);
            rbtn_type1.setVisibility(View.INVISIBLE);
            lay_mask.setVisibility(View.INVISIBLE);
            return;
        } else {
            rbtn_type0.setVisibility(View.VISIBLE);
            rbtn_type1.setVisibility(View.VISIBLE);
        }
        if (list_que_type.get(_cur_page - 1) == 0) {
            lay_mask.setVisibility(View.INVISIBLE);
            rbtn_type0.setChecked(true);
            rbtn_type1.setChecked(false);
            enable_que_view(_cur_page - 1);
        } else {
            lay_mask.setVisibility(View.VISIBLE);
            rbtn_type0.setChecked(false);
            rbtn_type1.setChecked(true);
            disable_que_view(_cur_page - 1);
        }
    }

    public void enable_que_view(int _que_idx) {
        list_et_item0.get(_que_idx).setEnabled(true);
        list_et_item1.get(_que_idx).setEnabled(true);
        list_et_item2.get(_que_idx).setEnabled(true);
        list_et_item3.get(_que_idx).setEnabled(true);
        list_et_item4.get(_que_idx).setEnabled(true);
    }

    public void disable_que_view(int _que_idx) {
        list_et_item0.get(_que_idx).setEnabled(false);
        list_et_item1.get(_que_idx).setEnabled(false);
        list_et_item2.get(_que_idx).setEnabled(false);
        list_et_item3.get(_que_idx).setEnabled(false);
        list_et_item4.get(_que_idx).setEnabled(false);
    }
}
