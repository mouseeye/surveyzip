package com.example.jkh.surveyzip.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jkh.surveyzip.DBHelper;
import com.example.jkh.surveyzip.FileHelper;
import com.example.jkh.surveyzip.R;

import java.io.File;
import java.util.ArrayList;

public class ResultDetailAct extends AppCompatActivity {
    DBHelper mHelper = new DBHelper(this);
    SQLiteDatabase db;

    int selected_idx; // 선택설문 IDX
    int total_survey_num; // 총 설문수

    Button btn_exit, btn_file;
    TextView tv_title, tv_date, tv_count;

    private ListView lv_list;
    private Item_Result_Detail_Adapter mlvadapter;
    private ArrayList<Item_Result_Detail> list_result = new ArrayList<Item_Result_Detail>();

    FileHelper mfilehelper = new FileHelper(this);
    final String f_path = "/mnt/sdcard/SurveyZip/";
    ArrayList<Integer> list_flag_dupchk;
    String f_contents = "";
    String f_name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_detail);

        Intent intent = getIntent();
        selected_idx = intent.getExtras().getInt("IDX");

        btn_exit = (Button) findViewById(R.id.btn_exit);
        btn_file = (Button) findViewById(R.id.btn_file);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_count = (TextView) findViewById(R.id.tv_count);
        lv_list = (ListView) findViewById(R.id.lv_list);

        if (list_result != null) list_result = new ArrayList<Item_Result_Detail>();

        mlvadapter = new Item_Result_Detail_Adapter(this, list_result);
        lv_list.setAdapter(mlvadapter);
        lv_list.setSelection(0);

        load_Survey_Result_Info();
        load_Survey_Result_Info_list();

        // '나가기' 버튼
        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // 'Text File' 버튼
        btn_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mfilehelper.mk_filepath(f_path + "/");
                f_name = mfilehelper.mk_txtfile(f_path + "/", f_contents);

                AlertDialog alert = new AlertDialog.Builder(ResultDetailAct.this)
                        .setTitle("결과파일 생성")
                        .setMessage("텍스트파일 생성이 완료되었습니다.\n\n저장경로 : " + f_path + f_name)
                        .setNegativeButton("닫기", null)
                        .setPositiveButton("파일열기", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                File file = new File(f_path + f_name);

                                // Intent 통한 txt 파일 열기
                                Intent intent = new Intent();
                                intent.setAction(android.content.Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(file), "text/*");
                                startActivity(intent);
                            }
                        }).show();
            }
        });
    }

    public void load_Survey_Result_Info() {
        String cur_date = "";
        db = mHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT  B.title, max(A.que_date), min(A.que_date), count(*), B.que_num " +
                "FROM survey_result A, survey_list B WHERE A.idx = B.idx and A.idx == " + selected_idx + ";", null);
        while (cursor.moveToNext()) {
            String title = cursor.getString(0);
            String date = cursor.getString(2).substring(5, 7) + "/" + cursor.getString(2).substring(8, 10)
                    + " ~ " + cursor.getString(1).substring(5, 7) + "/" + cursor.getString(1).substring(8, 10);
            int que_num = cursor.getInt(4);
            int result_num = cursor.getInt(3) / cursor.getInt(4);

            tv_title.setText(title);
            tv_date.setText("" + date);
            tv_count.setText("" + result_num);
            total_survey_num = result_num;

            // File Form 작성
            f_contents = f_contents + "--------------------------------------------------------\n\r";
            f_contents = f_contents + "[설문리포트] " + title + "\r\n";
            f_contents = f_contents + "- 설문인원 : " + result_num + "명\r\n";
            f_contents = f_contents + "- 설문기간 : " + date + "\r\n";
            f_contents = f_contents + "--------------------------------------------------------\n\r\n\r";

            if (list_flag_dupchk == null) list_flag_dupchk = new ArrayList<Integer>();
            list_flag_dupchk.clear();
            for (int i = 0; i < que_num; i++) {
                list_flag_dupchk.add(0);
            }
        }
    }

    public void load_Survey_Result_Info_list() {
        list_result.clear();

        // 설문 리스트 정보 검색
        db = mHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT que_idx, que_title, type, que_items  " +
                "FROM survey_que WHERE idx == " + selected_idx + " ORDER BY que_idx ASC;", null);
        while (cursor.moveToNext()) {
            String title = "질문" + (cursor.getInt(0) + 1) + ". " + cursor.getString(1);
            int type = cursor.getInt(2);
            String items = cursor.getString(3);

            list_result.add(new Item_Result_Detail(title, type, items));
        }
        mlvadapter.notifyDataSetChanged();
        lv_list.setSelection(0);

        load_Survey_Result_Info_result();
    }

    public void load_Survey_Result_Info_result() {
        // 설문결과 정보 검색 - '선택형'
        db = mHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT que_idx, type, result, count(*)  " +
                "FROM survey_result WHERE idx == " + selected_idx + " and type = 0 " +
                "GROUP BY que_idx, result ORDER BY que_idx ASC;", null);
        while (cursor.moveToNext()) {
            int que_idx = cursor.getInt(0);
            int type = cursor.getInt(1);
            int result_idx = Integer.parseInt(cursor.getString(2));
            int count = cursor.getInt(3);

            list_result.get(que_idx).list_result_type0.set(result_idx, count);
        }

        // 설문결과 정보 검색 - '입력형'
        cursor = db.rawQuery("SELECT que_idx, type, result  " +
                "FROM survey_result WHERE idx == " + selected_idx + " and type = 1 " +
                "GROUP BY que_idx, result ORDER BY que_idx ASC;", null);
        while (cursor.moveToNext()) {
            int que_idx = cursor.getInt(0);
            int type = cursor.getInt(1);
            String result = cursor.getString(2);

            // '입력형' 결과값 연달아 이어달기
            String str = list_result.get(que_idx).list_result_type1.get(que_idx);
            if (list_result.get(que_idx).list_result_type1.get(que_idx).equals("")) {
                list_result.get(que_idx).list_result_type1.set(que_idx, str + result);
            } else {
                list_result.get(que_idx).list_result_type1.set(que_idx, str + " / " + result);
            }
        }
        mlvadapter.notifyDataSetChanged();

        // File Form 작성
        for (int idx = 0; idx < list_result.size(); idx++) {
            if (list_result.get(idx).type == 0) {
                if (list_flag_dupchk != null && list_flag_dupchk.get(idx) == 0) {
                    // File Form 작성 - '선택형' 제목
                    f_contents = f_contents + list_result.get(idx).title + "\r\n";

                    String[] temp_string = list_result.get(idx).items.split("@@");
                    for (int i = 0; i < 5; i++) {
                        if (temp_string[i].equals("null")) {
                            break;
                        }
                        int value = list_result.get(idx).list_result_type0.get(i);
                        double percent = (double) list_result.get(idx).list_result_type0.get(i) / total_survey_num * 100;
                        String str_percent = String.format("%.1f", percent);
                        // File Form 작성 - '선택형' 결과정보
                        f_contents = f_contents + temp_string[i] + " " + value + "명 " + " " + str_percent + "%\r\n";
                    }
                    f_contents = f_contents + "\r\n";
                    list_flag_dupchk.set(idx, 1);
                }
            } else {
                if (list_flag_dupchk != null && list_flag_dupchk.get(idx) == 0) {
                    // File Form 작성 - '입력형' 제목
                    f_contents = f_contents + list_result.get(idx).title + "\r\n";
                    // File Form 작성 - '입력형' 결과정보
                    f_contents = f_contents + list_result.get(idx).list_result_type1.get(idx) + "\r\n\r\n";

                    list_flag_dupchk.set(idx, 1);
                }
            }
        }
    }

    class Item_Result_Detail {
        private String title;
        private int type;
        private String items;

        private int result;
        public ArrayList<Integer> list_result_type0; // '선택형' 결과리스트
        public ArrayList<String> list_result_type1; // '입력형' 결과리스트

        public Item_Result_Detail(String title, int type, String items) {
            this.title = title;
            this.type = type;
            this.items = items;
            list_result_type0 = new ArrayList<Integer>();
            list_result_type1 = new ArrayList<String>();
            for (int i = 0; i < 5; i++) {
                list_result_type0.add(0);
                list_result_type1.add("");
            }
        }
    }

    class Item_Result_Detail_Adapter extends BaseAdapter {
        LayoutInflater mInflater;
        ArrayList<Item_Result_Detail> arSrc;
        Context _context;

        public Item_Result_Detail_Adapter(Context context, ArrayList<Item_Result_Detail> item_list) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            arSrc = item_list;
            _context = context;
        }

        @Override
        public int getCount() {
            return arSrc.size();
        }

        @Override
        public Object getItem(int position) {
            return arSrc.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public ArrayList<Integer> selectedIds = new ArrayList<Integer>();

        public void toggleSelected(Integer position) {
            selectedIds.clear();
            selectedIds.add(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            int res_type0 = R.layout.lv_survey_result_detail_type0;
            int res_type1 = R.layout.lv_survey_result_detail_type1;
            int res_cur;
            if (arSrc.get(position).type == 0) { // '선택형' 항목
                res_cur = res_type0;
                convertView = mInflater.inflate(res_cur, parent, false);

                // 문항정보 출력
                ((TextView) convertView.findViewById(R.id.tv_que_title)).setText(arSrc.get(position).title);
                ArrayList<TextView> list_tv_item = new ArrayList<TextView>();
                ArrayList<TextView> list_tv_result = new ArrayList<TextView>();
                ArrayList<TextView> list_tv_percent = new ArrayList<TextView>();
                list_tv_item.add((TextView) convertView.findViewById(R.id.tv_item0));
                list_tv_item.add((TextView) convertView.findViewById(R.id.tv_item1));
                list_tv_item.add((TextView) convertView.findViewById(R.id.tv_item2));
                list_tv_item.add((TextView) convertView.findViewById(R.id.tv_item3));
                list_tv_item.add((TextView) convertView.findViewById(R.id.tv_item4));
                list_tv_result.add((TextView) convertView.findViewById(R.id.tv_result0));
                list_tv_result.add((TextView) convertView.findViewById(R.id.tv_result1));
                list_tv_result.add((TextView) convertView.findViewById(R.id.tv_result2));
                list_tv_result.add((TextView) convertView.findViewById(R.id.tv_result3));
                list_tv_result.add((TextView) convertView.findViewById(R.id.tv_result4));
                list_tv_percent.add((TextView) convertView.findViewById(R.id.tv_percent0));
                list_tv_percent.add((TextView) convertView.findViewById(R.id.tv_percent1));
                list_tv_percent.add((TextView) convertView.findViewById(R.id.tv_percent2));
                list_tv_percent.add((TextView) convertView.findViewById(R.id.tv_percent3));
                list_tv_percent.add((TextView) convertView.findViewById(R.id.tv_percent4));

                // 설문결과수치 출력
                String[] temp_string = arSrc.get(position).items.split("@@");
                for (int i = 0; i < 5; i++) {
                    if (temp_string[i].equals("null")) {
                        break;
                    }
                    list_tv_item.get(i).setText(temp_string[i]);
                    int value = arSrc.get(position).list_result_type0.get(i);
                    double percent = (double) arSrc.get(position).list_result_type0.get(i) / total_survey_num * 100;
                    String str_percent = String.format("%.1f", percent);
                    list_tv_result.get(i).setText("" + value + "명");
                    list_tv_percent.get(i).setText(str_percent + "%");
                }
            } else {// '입력형' 항목
                res_cur = res_type1;
                convertView = mInflater.inflate(res_cur, parent, false);

                // 문항정보 출력
                ((TextView) convertView.findViewById(R.id.tv_que_title)).setText(arSrc.get(position).title);
                ((TextView) convertView.findViewById(R.id.tv_result)).setText(arSrc.get(position).list_result_type1.get(position));
            }
            return convertView;
        }
    }
}
