package com.example.jkh.surveyzip;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.sql.Date;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.content.Context;
import android.util.Log;

public class FileHelper {
	Context context; 
	public FileHelper(Context _context) {
		context = _context;
	}

	// File - 디렉토리 생성
	public void mk_filepath( String path ){
		File file = new File(path);
		if ( !file.exists() ) {  file.mkdirs();  } // 디렉토리가 존재하지 않으면 디렉토리 생성
		return;
	}

	// File - txt File 생성
	public String mk_txtfile( String path, String content ){
		String cur_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(System.currentTimeMillis()));
		File file = new File(path+"report_"+cur_time+".txt");
		if ( !file.exists() ) { // 파일 존재하지 않으면 TXT 파일 생성
			try{
				FileOutputStream fos = new FileOutputStream(file);
				fos.write(content.getBytes());
				fos.close();
			} catch(IOException e){}

			String fname = "report_"+cur_time+".txt";
			return fname;
		}
		else{ return null; }
	}
}
