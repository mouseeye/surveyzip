package com.example.jkh.surveyzip.SurveyFragment;


import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jkh.surveyzip.DBHelper;
import com.example.jkh.surveyzip.R;
import com.example.jkh.surveyzip.UI.CustomViewPager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Do_Survey extends android.support.v4.app.Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    DBHelper mHelper;
    SQLiteDatabase db;

    private Handler handler = null;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Button btn_add;

    private List<Item_Survey> list_survey;

    private CustomViewPager viewPager;
    private PagerAdapterClass mPageAdpater;
    private int cur_page = 0;
    private int total_page;
    private ArrayList<EditText> list_et_input;

    private int selected_survey_idx;

    public int ans_idx = -1;
    public String ans_str = null;
    public ArrayList<String> list_answer; // 답변정보

    public static Do_Survey newInstance(String param1, String param2) {
        Do_Survey fragment = new Do_Survey();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public Do_Survey() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public void setHandler(Handler _handler) {
        handler = _handler;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mHelper = new DBHelper(getContext());

        View v = inflater.inflate(R.layout.fragment_do_survey, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        viewPager = (CustomViewPager) v.findViewById(R.id.view_pager);
        btn_add = (Button) v.findViewById(R.id.btn_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message msg = new Message();
                msg.what = 0;
                handler.sendMessage(msg);
            }
        });

        list_survey = new ArrayList<Item_Survey>();
        list_answer = new ArrayList<String>();
        list_et_input = new ArrayList<EditText>();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setScrollBarSize(0); // Hide Scrollbar
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new RecyclerAdapter(getContext(), list_survey, R.layout.activity_main);
        mRecyclerView.setAdapter(mAdapter);

        load_Survey_List();
        return v;
    }

    public void load_Survey_List() {
        list_survey.clear();

        db = mHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT idx, title, description, que_date FROM survey_list ORDER BY que_date DESC", null);
        while (cursor.moveToNext()) {
            int idx = cursor.getInt(0);
            Bitmap bm = null;
            String title = cursor.getString(1);
            String description = cursor.getString(2);
            String date = cursor.getString(3);

            list_survey.add(new Item_Survey(idx, bm, title, description, date));
        }
        mAdapter.notifyDataSetChanged();
        mRecyclerView.getLayoutManager().scrollToPosition(0);
    }

    public void load_Survey_Items(int idx) {
        db = mHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT que_idx, type, que_title, que_items FROM survey_que" +
                " WHERE idx = " + list_survey.get(idx).idx + " ORDER BY que_idx ASC;", null);

        list_survey.get(idx).init_list();
        int que_count = 0;
        while (cursor.moveToNext()) {
            list_survey.get(idx).list_que_type.add(cursor.getInt(1));
            list_survey.get(idx).list_que_title.add(cursor.getString(2));
            if (cursor.getInt(1) == 0) { // '선택형'
                String[] temp_string = cursor.getString(3).split("@@");
                list_survey.get(idx).list_que_item0.add(temp_string[0]);
                list_survey.get(idx).list_que_item1.add(temp_string[1]);
                list_survey.get(idx).list_que_item2.add(temp_string[2]);
                list_survey.get(idx).list_que_item3.add(temp_string[3]);
                list_survey.get(idx).list_que_item4.add(temp_string[4]);
            } else {// '입력형'
                list_survey.get(idx).list_que_item0.add(null);
                list_survey.get(idx).list_que_item1.add(null);
                list_survey.get(idx).list_que_item2.add(null);
                list_survey.get(idx).list_que_item3.add(null);
                list_survey.get(idx).list_que_item4.add(null);
            }
            que_count++;
        }
        total_page = que_count;
        list_et_input.clear();
        for( int i=0; i<total_page; i++){
            list_et_input.add(null);
        }

        viewPager.removeAllViews();
        mPageAdpater = new PagerAdapterClass(getContext());
        viewPager.setAdapter(mPageAdpater);
        cur_page = 0;
        viewPager.setCurrentItem(cur_page);

        mAdapter.notifyDataSetChanged();
    }

    public void delete_Survey_Items(int idx) {
        db = mHelper.getWritableDatabase();
        db.execSQL("DELETE FROM survey_que WHERE idx = " + list_survey.get(idx).idx + ";");
        db.execSQL("DELETE FROM survey_list WHERE idx = " + list_survey.get(idx).idx + ";");
        db.execSQL("DELETE FROM survey_result WHERE idx = " + list_survey.get(idx).idx + ";");

        // 리스트 갱신
        list_survey.remove(idx);
        mAdapter.notifyDataSetChanged();
        mRecyclerView.getLayoutManager().scrollToPosition(0);
    }

    public class Item_Survey {
        int idx;
        Bitmap bm;
        String title, description, date;

        public ArrayList<Integer> list_que_type; // 질문타입
        public ArrayList<String> list_que_title; // 질문명
        public ArrayList<String> list_que_item0; // 항목1
        public ArrayList<String> list_que_item1; // 항목2
        public ArrayList<String> list_que_item2; // 항목3
        public ArrayList<String> list_que_item3; // 항목4
        public ArrayList<String> list_que_item4; // 항목5

        Item_Survey(int idx, Bitmap bm, String title, String description, String date) {
            this.idx = idx;
            this.bm = bm;
            this.title = title;
            this.description = description;
            this.date = "등록일 : " + date;

            if (list_que_type == null) list_que_type = new ArrayList<Integer>();
            if (list_que_title == null) list_que_title = new ArrayList<String>();
            if (list_que_item0 == null) list_que_item0 = new ArrayList<String>();
            if (list_que_item1 == null) list_que_item1 = new ArrayList<String>();
            if (list_que_item2 == null) list_que_item2 = new ArrayList<String>();
            if (list_que_item3 == null) list_que_item3 = new ArrayList<String>();
            if (list_que_item4 == null) list_que_item4 = new ArrayList<String>();
        }

        public void init_list() {
            if (list_que_type != null) list_que_type.clear();
            if (list_que_title != null) list_que_title.clear();
            if (list_que_item0 != null) list_que_item0.clear();
            if (list_que_item1 != null) list_que_item1.clear();
            if (list_que_item2 != null) list_que_item2.clear();
            if (list_que_item3 != null) list_que_item3.clear();
            if (list_que_item4 != null) list_que_item4.clear();
        }
    }

    public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
        Context context;
        List<Item_Survey> items;
        int item_layout;

        public RecyclerAdapter(Context context, List<Item_Survey> items, int item_layout) {
            this.context = context;
            this.items = items;
            this.item_layout = item_layout;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_survey_form, parent, false);
            return new ViewHolder(v);
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            final Item_Survey item = items.get(position);
            holder.title.setText(item.title);
            holder.description.setText(item.description);
            holder.date.setText(item.date);
            holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selected_survey_idx = position;
                    load_Survey_Items(position); // 설문하기 정보 불러오기

                    mRecyclerView.setVisibility(View.INVISIBLE);
                    btn_add.setVisibility(View.INVISIBLE);
                    viewPager.setVisibility(View.VISIBLE);
                }
            });
            holder.btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog alert = new AlertDialog.Builder(getContext())
                            .setTitle("설문삭제")
                            .setMessage("설문정보를 삭제하시겠습니까?")
                            .setNegativeButton("닫기", null)
                            .setPositiveButton("삭제", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    delete_Survey_Items(position);
                                }
                            }).show();
                    return;
                }
            });
        }

        @Override
        public int getItemCount() {
            return this.items.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView title, description, date;
            CardView cardview;
            Button btn_delete;

            public ViewHolder(View itemView) {
                super(itemView);
                title = (TextView) itemView.findViewById(R.id.title);
                description = (TextView) itemView.findViewById(R.id.description);
                date = (TextView) itemView.findViewById(R.id.date);
                cardview = (CardView) itemView.findViewById(R.id.cardview);
                btn_delete = (Button) itemView.findViewById(R.id.btn_delete);
            }
        }
    }

    class PagerAdapterClass extends PagerAdapter {
        private LayoutInflater mInflater;
        Item_Survey cur_obj;

        public PagerAdapterClass(Context c) {
            super();
            mInflater = LayoutInflater.from(c);
            cur_obj = list_survey.get(selected_survey_idx);
        }

        @Override
        public int getCount() {
            return total_page;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

        @Override
        public Object instantiateItem(View pager, int position) {
            View v = null;
            ans_idx = -1;
            ans_str = null;

            if (cur_obj.list_que_type.get(position) == 0) { // '선택형' 문항
                v = mInflater.inflate(R.layout.inflate_survey_do_type0, null);

                final ArrayList<RadioButton> list_rbtn = new ArrayList<RadioButton>();
                ArrayList<LinearLayout> list_lay = new ArrayList<LinearLayout>();
                list_rbtn.add((RadioButton) v.findViewById(R.id.rbtn_item0));
                list_rbtn.add((RadioButton) v.findViewById(R.id.rbtn_item1));
                list_rbtn.add((RadioButton) v.findViewById(R.id.rbtn_item2));
                list_rbtn.add((RadioButton) v.findViewById(R.id.rbtn_item3));
                list_rbtn.add((RadioButton) v.findViewById(R.id.rbtn_item4));
                TextView tv_title = (TextView) v.findViewById(R.id.tv_title);
                TextView tv_item0 = (TextView) v.findViewById(R.id.tv_item0);
                TextView tv_item1 = (TextView) v.findViewById(R.id.tv_item1);
                TextView tv_item2 = (TextView) v.findViewById(R.id.tv_item2);
                TextView tv_item3 = (TextView) v.findViewById(R.id.tv_item3);
                TextView tv_item4 = (TextView) v.findViewById(R.id.tv_item4);
                LinearLayout lv_item0 = (LinearLayout) v.findViewById(R.id.lv_item0);
                LinearLayout lv_item1 = (LinearLayout) v.findViewById(R.id.lv_item1);
                LinearLayout lv_item2 = (LinearLayout) v.findViewById(R.id.lv_item2);
                LinearLayout lv_item3 = (LinearLayout) v.findViewById(R.id.lv_item3);
                LinearLayout lv_item4 = (LinearLayout) v.findViewById(R.id.lv_item4);
                list_lay.add(lv_item0);
                list_lay.add(lv_item1);
                list_lay.add(lv_item2);
                list_lay.add(lv_item3);
                list_lay.add(lv_item4);
                list_et_input.set(position, null);

                tv_title.setText("질문" + (position + 1) + ". " + cur_obj.list_que_title.get(position));
                if (!cur_obj.list_que_item0.get(position).equals("null"))
                    tv_item0.setText(cur_obj.list_que_item0.get(position));
                else lv_item0.setVisibility(View.INVISIBLE);
                if (!cur_obj.list_que_item1.get(position).equals("null"))
                    tv_item1.setText(cur_obj.list_que_item1.get(position));
                else lv_item1.setVisibility(View.INVISIBLE);
                if (!cur_obj.list_que_item2.get(position).equals("null"))
                    tv_item2.setText(cur_obj.list_que_item2.get(position));
                else lv_item2.setVisibility(View.INVISIBLE);
                if (!cur_obj.list_que_item3.get(position).equals("null"))
                    tv_item3.setText(cur_obj.list_que_item3.get(position));
                else lv_item3.setVisibility(View.INVISIBLE);
                if (!cur_obj.list_que_item4.get(position).equals("null"))
                    tv_item4.setText(cur_obj.list_que_item4.get(position));
                else lv_item4.setVisibility(View.INVISIBLE);

                for (int i = 0; i < list_lay.size(); i++) {
                    list_lay.get(i).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            for (int i = 0; i < list_rbtn.size(); i++)
                                list_rbtn.get(i).setChecked(false);
                            list_rbtn.get(getViewIDX(v)).setChecked(true);

                            ans_idx = getViewIDX(v); // 답변입력
                        }
                    });
                }
            } else {// '입력형' 문항
                v = mInflater.inflate(R.layout.inflate_survey_do_type1, null);

                TextView tv_title = (TextView) v.findViewById(R.id.tv_title);
                list_et_input.set(position, (EditText) v.findViewById(R.id.et_input));
                tv_title.setText("질문" + (position + 1) + ". " + cur_obj.list_que_title.get(position));
            }

            // '나가기' 버튼
            Button btn_exit = (Button) v.findViewById(R.id.btn_exit);
            btn_exit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btn_add.setVisibility(View.VISIBLE);

                    viewPager.removeAllViews();
                    viewPager.setVisibility(View.INVISIBLE);

                    mRecyclerView.setVisibility(View.VISIBLE);
                    mRecyclerView.bringToFront();
                }
            });

            // '다음문항' 버튼
            Button btn_next = (Button) v.findViewById(R.id.btn_next);
            btn_next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (list_et_input.get(cur_page) != null)
                        ans_str = list_et_input.get(cur_page).getText().toString().trim(); // 답변입력

                    if (checkValue() == -1) {
                        Toast.makeText(getContext(), "항목을 선택해주세요.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (checkValue() == -2) {
                        Toast.makeText(getContext(), "항목을 입력해주세요.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    inputValue();

                    cur_page = cur_page + 1;

                    // 설문이 완료된 경우
                    if (cur_page >= total_page) {
                        db = mHelper.getWritableDatabase();
                        String cur_time = "" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
                        for (int i = 0; i < list_answer.size(); i++) {
                            db.execSQL("INSERT into survey_result (idx, que_date, que_idx, type, result) values " +
                                    "(" + cur_obj.idx + ", '" + cur_time + "', '" + i + "', " + cur_obj.list_que_type.get(i) + ",'" + list_answer.get(i) + "')");
                        }

                        AlertDialog alert = new AlertDialog.Builder(getContext())
                                .setTitle("설문하기")
                                .setMessage("설문이 완료되었습니다.")
                                .setPositiveButton("메인으로", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        mRecyclerView.setVisibility(View.VISIBLE);
                                        mRecyclerView.bringToFront();
                                        btn_add.setVisibility(View.VISIBLE);

                                        // ViewPager 초기화
                                        viewPager.removeAllViews();
                                        viewPager.setVisibility(View.INVISIBLE);

                                        hide_keyboard_all();

                                        list_answer.clear();
                                        list_et_input.clear();
                                    }
                                }).show();
                        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                mRecyclerView.setVisibility(View.VISIBLE);
                                mRecyclerView.bringToFront();
                                btn_add.setVisibility(View.VISIBLE);

                                // ViewPager 초기화
                                viewPager.removeAllViews();
                                viewPager.setVisibility(View.INVISIBLE);

                                hide_keyboard_all();

                                list_answer.clear();
                                list_et_input.clear();
                            }
                        });
                        return;
                    }

                    // 다음 페이지 이동
                    if (cur_page >= total_page) {
                        cur_page = total_page - 1;
                    }
                    viewPager.setCurrentItem(cur_page);
                    hide_keyboard_all();
                }
            });

            ((CustomViewPager) pager).addView(v, 0);
            return v;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public boolean isViewFromObject(View pager, Object obj) {
            return pager == obj;
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
        }

        @Override
        public void destroyItem(View pager, int position, Object view) {
            if (position == 0) {
            }
            ((CustomViewPager) pager).removeView((View) view);
        }

        @Override
        public void finishUpdate(View arg0) {
        }

        public int getViewIDX(View v) {
            switch (v.getId()) {
                case R.id.lv_item0:
                    return 0;
                case R.id.lv_item1:
                    return 1;
                case R.id.lv_item2:
                    return 2;
                case R.id.lv_item3:
                    return 3;
                case R.id.lv_item4:
                    return 4;
                default:
                    return -1;
            }
        }

        public int checkValue() {
            if (cur_obj.list_que_type.get(cur_page) == 0 && ans_idx == -1) {
                return -1;
            } else if (cur_obj.list_que_type.get(cur_page) == 1 && ans_str.equals("")) {
                return -2;
            }
            return 1;
        }

        public void inputValue() {
            if (cur_obj.list_que_type.get(cur_page) == 0) {
                list_answer.add("" + ans_idx);
            } else {
                list_answer.add("" + ans_str);
            }
        }
    }

    public void hide_keyboard_all() {
        if (list_et_input != null) {
            for (int i = 0; i < list_et_input.size(); i++) {
                hide_keyboard(list_et_input.get(i));
            }
        }
    }

    public void hide_keyboard(EditText et) {
        if (et == null) return;
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
    }
}
