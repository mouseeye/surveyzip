package com.example.jkh.surveyzip.SurveyFragment;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jkh.surveyzip.DBHelper;
import com.example.jkh.surveyzip.R;

import java.util.ArrayList;

public class Result_Survey extends android.support.v4.app.Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    DBHelper mHelper;
    SQLiteDatabase db;

    private Handler handler = null;

    private ListView lv_list;
    private Item_Result_Adapter item_result_adapter;
    private ArrayList<Item_Result> list_result = new ArrayList<Item_Result>();

    public static Result_Survey newInstance(String param1, String param2) {
        Result_Survey fragment = new Result_Survey();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public Result_Survey() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public void setHandler(Handler _handler) {
        handler = _handler;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mHelper = new DBHelper(getContext());
        View v = inflater.inflate(R.layout.fragment_result_survey, container, false);

        lv_list = (ListView) v.findViewById(R.id.lv_list);

        if( list_result != null) list_result = new ArrayList<Item_Result>();
        lv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Message msg = new Message();
                msg.what = list_result.get(position).idx;
                handler.sendMessage(msg);
            }
        });

        // ListView Adapter 설정
        item_result_adapter = new Item_Result_Adapter(getContext(), list_result);
        lv_list.setAdapter(item_result_adapter);
        lv_list.setSelection(0);

        load_Survey_Result_List();
        return v;
    }

    public void load_Survey_Result_List() {
        list_result.clear();

        String cur_date = "";
        db = mHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT A.idx, B.title, max(A.que_date), min(A.que_date), count(*), B.que_num " +
                "FROM survey_result A, survey_list B " +
                "WHERE A.idx = B.idx GROUP BY A.idx ORDER BY max(A.que_date) DESC", null);
        while (cursor.moveToNext()) {
            int idx = cursor.getInt(0);
            String title = cursor.getString(1);
            String date = cursor.getString(3).substring(5,7) + "/" + cursor.getString(3).substring(8,10)
                    + " ~ " + cursor.getString(2).substring(5,7) + "/" + cursor.getString(2).substring(8,10);
            int result_num = cursor.getInt(4) /  cursor.getInt(5);

            if( cur_date.equals("") || !cur_date.equals(cursor.getString(2).substring(0,10) ) ) {
                cur_date = cursor.getString(2).substring(0,10);
                list_result.add(new Item_Result(0, -1, null, cur_date, -1));
            }
            list_result.add(new Item_Result(1, idx, title, date, result_num));
        }
        item_result_adapter.notifyDataSetChanged();
        lv_list.setSelection(0);
    }

    class Item_Result {
        private int item_type; // 0: 날짜, 1: 설문리스트
        private int idx;
        private Bitmap bm;
        private String title;
        private String date;
        private int count;

        public Item_Result(int item_type, int idx, String title, String date, int count ) {
            this.item_type = item_type;
            this.idx = idx;
            this.title = title;
            this.date = date;
            this.count = count;
        }
        public void set_bm(Bitmap _bm) {
            bm = _bm;
        }
    }

    class Item_Result_Adapter extends BaseAdapter {
        LayoutInflater mInflater;
        ArrayList<Item_Result> arSrc;
        Context _context;

        public Item_Result_Adapter(Context context, ArrayList<Item_Result> item_list) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            arSrc = item_list;
            _context = context;
        }

        @Override
        public int getCount() {
            return arSrc.size();
        }

        @Override
        public Object getItem(int position) {
            return arSrc.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public ArrayList<Integer> selectedIds = new ArrayList<Integer>();

        public void toggleSelected(Integer position) {
            selectedIds.clear();
            selectedIds.add(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            int res_date = R.layout.lv_survey_result_0;
            int res_result = R.layout.lv_survey_result_1;
            int res_cur;

            if (arSrc.get(position).item_type == 0) { // Date Item 경우
                res_cur = res_date;
                convertView = mInflater.inflate(res_cur, parent, false);

                ( (TextView) convertView.findViewById(R.id.tv_date) ).setText( arSrc.get(position).date );;
            }
            else { // Result Item 경우
                res_cur = res_result;
                convertView = mInflater.inflate(res_cur, parent, false);

                ( (TextView) convertView.findViewById(R.id.tv_title) ).setText( arSrc.get(position).title );
                ( (TextView) convertView.findViewById(R.id.tv_date) ).setText( arSrc.get(position).date );
                ( (TextView) convertView.findViewById(R.id.tv_count) ).setText( "" + arSrc.get(position).count +"명");
            }
            return convertView;
        }
    }
}
