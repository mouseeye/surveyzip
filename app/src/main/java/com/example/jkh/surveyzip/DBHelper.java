package com.example.jkh.surveyzip;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.Date;


public class DBHelper extends SQLiteOpenHelper {
    // DB 파일 생성
    public DBHelper(Context context) {
        super(context, "db_surveyzip.db", null, 1);
    }

    // DB 처음 만들어 질때 호출.
    public void onCreate(SQLiteDatabase db) {
        // survey_list 테이블 생성
        db.execSQL("CREATE TABLE survey_list (idx INTEGER, title VARCHAR, description VARCHAR, que_date VARCHAR, que_num INTEGER," +
                "CONSTRAINT survey_list_PK PRIMARY KEY (idx));");

        // survey_que 테이블 생성
        db.execSQL("CREATE TABLE survey_que (idx INTEGER, que_idx INTEGER, type INTEGER, que_title VARCHAR, que_items VARCHAR, " +
                "CONSTRAINT survey_que_FK FOREIGN KEY (idx) REFERENCES survey_list(idx));");

        // survey_result 테이블 생성
        db.execSQL("CREATE TABLE survey_result (idx INTEGER, que_date VARCHAR, que_idx INTEGER, type INTEGER, result VARCHAR, " +
                "CONSTRAINT survey_result_FK FOREIGN KEY (idx) REFERENCES survey_list(idx));");

        // 설문 리스트 정보 입력 (Sample)
        db.execSQL("INSERT into survey_list (idx, title, description, que_date, que_num) values " +
                "(0, '구매 관련 설문', '2015년 10월 구매관련 설문입니다. 소중한 의견을 들려주세요.'," +
                "'2015-10-26 23:00:00', 5)");

        // 설문 문항 정보 입력
        db.execSQL("INSERT into survey_que (idx, que_idx, type, que_title, que_items) values " +
                "(0, 0, 0, '다음 중 본인이 가장 최근에 구입한 제품을 선택하세요.', " +
                "'가방/핸드백@@모자@@넥타이@@벨트@@신발');");
        db.execSQL("INSERT into survey_que (idx, que_idx, type, que_title, que_items) values " +
                "(0, 1, 0, '평소 패션잡화를 어디에서 구입하십니까?', " +
                "'백화점@@로드샵@@해외직구@@TV홈쇼핑@@아울렛');");
        db.execSQL("INSERT into survey_que (idx, que_idx, type, que_title, que_items) values " +
                "(0, 2, 0, '구매에 소요되는 시간은?', " +
                "'30분 이내@@30분~1시간@@1시간~2시간@@2시간~3시간@@3시간 이상');");
        db.execSQL("INSERT into survey_que (idx, que_idx, type, que_title, que_items) values " +
                "(0, 3, 0, '최근 한 달 사이에 구매한 횟수는?', " +
                "'없음@@1회@@2회@@3회@@4회 이상');");
        db.execSQL("INSERT into survey_que (idx, que_idx, type, que_title, que_items) values " +
                "(0, 4, 1, '구매 관련 기타의견사항을 기재해 주세요.', " +
                "'null');");

        String cur_time = "2015-10-26 23:00:00";
        db.execSQL("INSERT into survey_result (idx, que_date, que_idx, type, result) values " +
                "(0, '" + cur_time + "', '0', 0,'0')");
        db.execSQL("INSERT into survey_result (idx, que_date, que_idx, type, result) values " +
                "(0, '" + cur_time + "', '1', 0,'1')");
        db.execSQL("INSERT into survey_result (idx, que_date, que_idx, type, result) values " +
                "(0, '" + cur_time + "', '2', 0,'2')");
        db.execSQL("INSERT into survey_result (idx, que_date, que_idx, type, result) values " +
                "(0, '" + cur_time + "', '3', 0,'3')");
        db.execSQL("INSERT into survey_result (idx, que_date, que_idx, type, result) values " +
                "(0, '" + cur_time + "', '4', 1,'주말엔 쇼핑이 제맛!')");
    }

    // DB 버전이 바뀌어 업그레이드 할때 호출
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS dic");
        onCreate(db);

    }
}
